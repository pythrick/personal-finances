export class Transaction {
    uuid: string;
    description: string;
    amount: string;
    formatted_amount: string;
    transaction_type: string;
}

export class TransactionList {
  transactions: Transaction[];
  balance: string;
}
