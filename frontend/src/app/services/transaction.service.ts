import {Injectable} from '@angular/core';
import {HttpService} from "./http.service";
import {Transaction} from "../models/transaction.model";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private httpService: HttpService) {
  }

  retrieve(uuid: string) {
    return this.httpService.get("/transactions/" + uuid + "/");
  }

  list(ordering: string=null) {
    let url = "/transactions/";
    if(ordering){
      url += '?ordering=' + ordering;
    }
    return this.httpService.get(url);
  }

  create(transaction: Transaction) {
    return this.httpService.post(
      "/transactions/",
      transaction
    );
  }

  remove(transaction: Transaction) {
    return this.httpService.delete(
      "/transactions/" + transaction.uuid + "/"
    );
  }

  update(transaction: Transaction) {
    return this.httpService.put(
      "/transactions/" + transaction.uuid + "/",
      transaction
    );
  }

  partial_update(transaction, data: any) {
    return this.httpService.patch(
      "/transactions/" + transaction.uuid + "/",
      data
    );
  }
}
