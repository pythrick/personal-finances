from babel.numbers import format_currency
from django.db.models import Sum
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .serializers import TransactionSerializer
from .models import Transaction


class TransactionViewSet(ModelViewSet):
    """
    A simple ModelViewSet for listing or creating new transactions.
    """

    serializer_class = TransactionSerializer
    filter_fields = ('amount', 'description')
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    pagination_class = None
    lookup_field = 'uuid'

    def get_queryset(self):
        return Transaction.objects.filter(owner=self.request.user).all()

    def get_serializer_context(self):
        context = super(TransactionViewSet, self).get_serializer_context()
        context.update(
            {
                'owner': self.request.user
            }
        )
        return context

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        balance = queryset.aggregate(balance=Sum('amount'))["balance"]
        formatted_balance = format_currency(balance, currency="BRL", locale="pt_BR") if balance else None
        response = {
            "transactions": serializer.data,
            "balance": formatted_balance
        }
        return Response(response)
