import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField


class TransactionCategory(models.Model):
    name = models.CharField(_('Name'), max_length=100, null=False, blank=False)
    slug = AutoSlugField(populate_from='name', null=False, blank=False, unique=True,)

    created_at = models.DateTimeField(_('Created Date'), auto_now_add=True, null=False, blank=False)
    updated_at = models.DateTimeField(_('Updated Date'), auto_now=True, null=True, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        app_label = 'transaction'
        db_table = 'transactions_categories'
        ordering = ('created_at',)
        verbose_name = _('Transaction Category')
        verbose_name_plural = _('Transactions Categories')


class Transaction(models.Model):
    EARNING_TYPE = 'earning'
    EXPENSE_TYPE = 'expense'
    TRANSACTION_TYPES = (
        (EARNING_TYPE, _('Earning')),
        (EXPENSE_TYPE, _('Expense'))
    )

    uuid = models.UUIDField(default=uuid.uuid4, editable=False, null=False, blank=False, unique=True)
    description = models.CharField(_('Description'), max_length=150, null=False, blank=False)
    amount = models.DecimalField(_('Amount'), max_digits=8, decimal_places=2, null=False, blank=False)
    transaction_type = models.CharField(
        _('Type'), max_length=10, null=False, blank=False, choices=TRANSACTION_TYPES, default=EARNING_TYPE
    )
    owner = models.ForeignKey('user.User', models.CASCADE, 'transactions', null=False)
    category = models.ForeignKey('transaction.TransactionCategory', models.CASCADE, 'transactions', null=True)

    created_at = models.DateTimeField(_('Created Date'), auto_now_add=True, null=False, blank=False)
    updated_at = models.DateTimeField(_('Updated Date'), auto_now=True, null=True, blank=False)

    def __str__(self):
        return f"{self.owner}: {self.amount}"

    class Meta:
        app_label = 'transaction'
        db_table = 'transactions'
        ordering = ('created_at',)
        verbose_name = _('Transaction')
        verbose_name_plural = _('Transactions')
